<?php

namespace App\Domain\Repository;

use App\Domain\Model\Game;

interface GameRepositoryInterface
{
    public function save(Game $game);
}