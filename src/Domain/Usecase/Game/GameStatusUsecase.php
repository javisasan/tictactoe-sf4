<?php

namespace App\Domain\Usecase\Game;

class GameStatusUsecase extends AbstractGameUsecase
{
    public function execute() {

        // Iterate through possible correct lines combinations
        foreach ($this->game->getBoard()->getValidRows() as $checkPositions) {
            $pos1 = intval($checkPositions[0]);
            $pos2 = intval($checkPositions[1]);
            $pos3 = intval($checkPositions[2]);

            // Retrieve current grid data
            $grid = $this->game->getBoard()->getGrid();

            // Check if we have a winner!
            if (!empty($grid[$pos1]) && !empty($grid[$pos2]) && !empty($grid[$pos3])) {
                if ($grid[$pos1] == $grid[$pos2] && $grid[$pos1] == $grid[$pos3]) {
                    $this->game->setGameFinished();
                    $userWinner = $this->game->getCurrentUser();
                    $this->game->setWinner($userWinner);
                }
            }
        }

        return [
            'finished'  => $this->game->isFinished(),
            'winner'    => $this->game->getWinner()
        ];
    }
}