<?php

namespace App\Domain\Usecase\Game;

use App\Domain\Exception\CellNotEmptyException;
use App\Domain\Exception\IllegalMoveException;
use App\Domain\Exception\InvalidPositionException;

class AddMovementUsecase extends AbstractGameUsecase
{
    public function execute($position) {

        // Check game is not over
        if ($this->game->isFinished() == true) {
            throw new IllegalMoveException('Game is already over!');
        }

        // Get grid array key position
        $position = strtoupper($position);
        $gridPosition = $this->game->getBoard()->getGridPosition($position);

        // Check position exists
        if ($gridPosition === null) {
            throw new InvalidPositionException('Invalid position!');
        }

        // Check position is not empty
        if (!empty($this->game->getBoard()->getGridValue($gridPosition))) {
            throw new CellNotEmptyException('This position is not empty');
        }

        // Set grid value at the position
        $this->game->getBoard()->setGridValue($gridPosition, (($this->game->getMovements() % 2) > 0 ? 'X' : 'O'));

        // Update counters and next turn player
        $this->game->addMovement();

        // Board is full and there is no winner (deuce)
        if ($this->game->getMovements() == count($this->game->getBoard()->getGrid())) {
            $this->game->setGameFinished();
        }

        return true;
    }
}