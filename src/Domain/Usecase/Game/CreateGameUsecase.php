<?php

namespace App\Domain\Usecase\Game;

use App\Domain\Model\Board;
use App\Domain\Model\Game;
use App\Domain\Model\User;
use App\Domain\Repository\GameRepositoryInterface;

class CreateGameUsecase
{
    private $gameRepositoryInterface;

    public function __construct(GameRepositoryInterface $gameRepositoryInterface) {
        $this->gameRepositoryInterface = $gameRepositoryInterface;
    }

    public function execute(User $user1, User $user2) {
        $board = new Board();
        $game = new Game($user1, $user2, $board);

        $this->gameRepositoryInterface->save($game);

        return $game;
    }
}