<?php

namespace App\Domain\Usecase\User;

use App\Domain\Model\User;
use App\Domain\Repository\UserRepositoryInterface;

class DeleteUserUsecase
{
    private $userRepositoryInterface;
    private $user;

    public function __construct(UserRepositoryInterface $userRepositoryInterface, User $user) {
        $this->userRepositoryInterface = $userRepositoryInterface;
        $this->user                 = $user;
    }

    public function execute($user) {
        $status = $this->userRepositoryInterface->delete($user);

        return $status;
    }
}