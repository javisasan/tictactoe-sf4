<?php

namespace App\Domain\Exception;

class InvalidPositionException extends IllegalMoveException
{
}