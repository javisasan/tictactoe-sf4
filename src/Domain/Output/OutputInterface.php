<?php

namespace App\Domain\Output;

interface OutputInterface
{
    public function outputGrid();
}