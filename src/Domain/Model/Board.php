<?php

namespace App\Domain\Model;

class Board
{
    // Winning rows
    const VALID_ROWS = ['012', '345', '678', '036', '147', '258', '048', '246'];

    // Grid coordinates and array positions
    const GRID_POSITIONS = [
        '1A' => 0,
        '2A' => 1,
        '3A' => 2,
        '1B' => 3,
        '2B' => 4,
        '3B' => 5,
        '1C' => 6,
        '2C' => 7,
        '3C' => 8
    ];

    /**
     * @var array
     */
    private $grid;

    public function __construct()
    {
        $this->grid = array_fill(0, 9, null);
    }

    /**
     * This method returns all the grid
     *
     * @return array
     */
    public function getGrid()
    {
        return $this->grid;
    }

    /**
     * This method returns the array index from the grid coordinates received
     *
     * @param $position
     * @return int|null
     */
    public function getGridPosition($position): ?int
    {
        if (isset(self::GRID_POSITIONS[$position])) {
            return self::GRID_POSITIONS[$position];
        }
        return null;
    }

    /**
     * Returns the value of the grid position
     *
     * @param $position
     * @return null|string
     */
    public function getGridValue($position): ?string
    {
        return $this->grid[$position];
    }

    /**
     * Sets the value of the grid position
     * @param $position
     * @param $value
     */
    public function setGridValue($position, $value): void
    {
        $this->grid[$position] = $value;
    }

    /**
     * Returns all the valid Tic Tac Toe possible lines
     * @return array
     */
    public function getValidRows(): array
    {
        return self::VALID_ROWS;
    }
}