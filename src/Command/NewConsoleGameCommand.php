<?php

namespace App\Command;

use App\Domain\Usecase\Game\AddMovementUsecase;
use App\Domain\Usecase\Game\CreateGameUsecase;
use App\Domain\Usecase\Game\GameStatusUsecase;
use App\Domain\Usecase\User\CreateUserUsecase;
use App\Infrastructure\Output\ConsoleOutput;
use App\Infrastructure\Repository\Game\GameMongoRepository;
use App\Infrastructure\Repository\User\UserMongoRepository;
use App\Infrastructure\Repository\User\UserMySqlRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NewConsoleGameCommand extends Command
{
    protected function configure()
    {
        $this->setName('ttt:new-game');

        $this->setDescription('Creates a new console game.');

        $this->setHelp('This command creates a new game and lets a couple of users play.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            do {
                // Create users
                $userManager1 = new CreateUserUsecase(new UserMySqlRepository());
                $user1 = $userManager1->execute(readline('Name of the first user (O): '));
                $userManager2 = new CreateUserUsecase(new UserMongoRepository());
                $user2 = $userManager2->execute(readline('Name of the first user (X): '));

                // Create game
                $gameManager = new CreateGameUsecase(new GameMongoRepository());
                $game = $gameManager->execute($user1, $user2);

                // Print grid
                $output = new ConsoleOutput($game);
                $output->outputGrid();

                // Input movements and print grid, iterate
                $movementManager = new AddMovementUsecase($game);
                $statusManager = new GameStatusUsecase($game);
                while (!$game->isFinished()) {
                    try {
                        $movementManager->execute(readline('Enter ' . $game->getNextUser()->getName() . ' movement (for example, 2b) : '));
                        $statusManager->execute();
                        $output->outputGrid();
                    } catch (\Exception $e) {
                        echo $e->getMessage() . PHP_EOL;
                    }
                }
            } while (strtoupper(readline('Try again? (y/n) ')) == 'Y');
        } catch (\Exception $e) {
            echo 'ERROR: ' . $e->getMessage() . "\n";
        }
    }
}