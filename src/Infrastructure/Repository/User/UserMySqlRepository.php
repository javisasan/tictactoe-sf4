<?php

namespace App\Infrastructure\Repository\User;

use App\Domain\Model\User;
use App\Domain\Repository\UserRepositoryInterface;

class UserMySqlRepository implements UserRepositoryInterface
{
    public function save(User $user) {
        echo "*** User should be stored in MySQL!\n";
    }

    public function delete(User $user) {
        echo "*** User should be deleted from MySQL!\n";
    }
}