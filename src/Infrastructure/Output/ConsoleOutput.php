<?php

namespace App\Infrastructure\Output;

use App\Domain\Model\Game;
use App\Domain\Output\OutputInterface;

class ConsoleOutput implements OutputInterface
{
    /**
     * @var Game
     */
    private $game;

    public function __construct(Game $game) {
        $this->game = $game;
    }

    public function outputGrid() {
        $grid = $this->game->getBoard()->getGrid();

        // Convert null spaces to blank whitespaces in order to output through console
        foreach ($grid as $key => $pos) {
            if (!$grid[$key]) {
                $grid[$key] = ' ';
            }
        }

        // Draw a console grid with users data
        echo "  1 2 3\n";
        echo " +-+-+-+\n";
        echo "A|".$grid[0]."|".$grid[1]."|".$grid[2]."|\n";
        echo " +-+-+-+\n";
        echo "B|".$grid[3]."|".$grid[4]."|".$grid[5]."|\n";
        echo " +-+-+-+\n";
        echo "C|".$grid[6]."|".$grid[7]."|".$grid[8]."|\n";
        echo " +-+-+-+\n";

        // If game is finished, show the winner
        if ($this->game->isFinished()) {
            echo 'Game finished! Winner: ' . (!empty($this->game->getWinner()) ? $this->game->getWinner()->getName() : 'DEUCE') . "\n";
        }
    }
}