<?php

use PHPUnit\Framework\TestCase;
use App\Domain\Model\User;
use App\Domain\Usecase\User\DeleteUserUsecase;
use App\Infrastructure\Repository\User\UserMySqlRepository;

class DeleteUserUsecaseTest extends TestCase
{
    private $testUser;

    /**
     * @before
     */
    public function setupFixture() {
        $this->testUser = new User('testUsername');
    }

    public function testExecute() {
        // Stub in order to avoid to delete from DB
        $repositoryStub = $this->createMock(UserMySqlRepository::class);
        $repositoryStub->method("delete")
            ->willReturn(true);

        // Test the user deletion
        $deleteUserUsecase = new DeleteUserUsecase($repositoryStub, $this->testUser);
        $status = $deleteUserUsecase->execute($this->testUser);

        $this->assertEquals(true, $status);
    }

    public function testExecuteFailed() {
        // Stub in order to avoid to delete from DB. Let's say it fails.
        $repositoryStub = $this->createMock(UserMySqlRepository::class);
        $repositoryStub->method("delete")
            ->willReturn(false);

        // Test the user deletion
        $deleteUserUsecase = new DeleteUserUsecase($repositoryStub, $this->testUser);
        $status = $deleteUserUsecase->execute($this->testUser);

        $this->assertNotEquals(true, $status);
    }

}
